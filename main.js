export function setup(ctx) {
    const id = 'FILL';
    const title = 'FILL';
    const desc = "FILL";
    const imgSrc = 'assets/media/bank/rune_blood.png';

    // Setup the settings menu
    ctx.settings.section(title).add({
        name:id,
        type:'switch',
        label:'FILL?',
        hint:'FILL',
        default:true
    });
}
